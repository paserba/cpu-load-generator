# main.py

from fastapi import BackgroundTasks, FastAPI
from cpu_load_generator import load_all_cores

app = FastAPI()

def generate_load():
    print("Generate laod")
    load_all_cores(duration_s=2, target_load=0.5)
    print("Load End")

@app.get("/healthz")
async def root():
    return {"message": "Hello World"}

@app.get("/")
async def root(background_tasks :  BackgroundTasks):
    background_tasks.add_task(generate_load)
    return {"message": "Hello World"}