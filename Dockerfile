FROM python:3
RUN mkdir /app
WORKDIR /app
ADD main.py .
ADD requirements.txt .
RUN pip install -r requirements.txt
CMD ["/bin/sh", "-c", "uvicorn main:app --host 0.0.0.0 --port 80" ]